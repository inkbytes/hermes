# Use the official Node.js image as the base image
# You can use any version that fits your usecase
FROM node:18

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
# Copy the rest of the application code into the container
COPY . /usr/src/app

# Expose the port that the application listens on
EXPOSE 3000

# Start the application
CMD [ "npm", "run", "start" ]
