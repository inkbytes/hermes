// your.service.ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RabbitMQModule, RabbitSubscribe } from "@golevelup/nestjs-rabbitmq";
import { Ctx, EventPattern, MessagePattern, Payload, RmqContext } from "@nestjs/microservices";
import { LogMessage } from '@entities/messor';
import * as fs from 'fs';
import * as util from 'util';
const writeFile = util.promisify(fs.writeFile);
const filePath = './received_messages.txt';
@Injectable()
export class PgService {
  _messageContent: any;
  constructor(
    @InjectRepository(LogMessage)
    private readonly messorRepository: Repository<LogMessage>,
  ) {
    writeFile(filePath, JSON.stringify('hello') + '\n', {
      flag: 'a',
    }).then(() => {
      console.log('done');
    });
  }

  /*@RabbitSubscribe({ queue: 'hermes' })
  async handleMessage(messageContent: any): Promise<void> {
    this._messageContent = messageContent;
    // Your handling logic
    await writeFile(filePath, JSON.stringify(messageContent) + '\n', {
      flag: 'a',
    });
  }*/

  /*@RabbitSubscribe({ queue: 'hermes' })
  getNotifications(@Payload() data: number[], @Ctx() context: RmqContext) {
    console.log(`Pattern: ${context.getPattern()}`);
  }*/
  @RabbitSubscribe({ queue: 'hermes' })
  async handlePGMessage(messageContent: any): Promise<void> {
    const processedData = this.processData(messageContent.data);
    processedData.id = 0;
    console.log(processedData);
    await this.messorRepository.save(processedData);
    await writeFile(filePath, JSON.stringify(processedData) + '\n', {
      flag: 'a',
    });
  }

  private processData(messageContent: any): LogMessage {
    this._messageContent = messageContent;
    console.log(messageContent);
    const processedData = LogMessage.createFromJSON(messageContent);
    return processedData;
    // Process your message content here
    //return new LogMessage(); // Return an instance of your entity filled with processed data
  }
}
