import { Controller, Get } from '@nestjs/common';
import { AppService } from '@services/app.service';
import { DigitalOceanService } from 'src/digitalocean/digitalocean.service';
import { FileElement } from '@entities/file-element';
import { get } from 'http';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly doService: DigitalOceanService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Get('/dofiles')
  async listFiles(): Promise<FileElement[]> {
    const fileElements = await this.doService.listFiles();

    // Transform the list of files into FileElement objects asynchronously
    //const fileElements = await Promise.all(
     // files.map((item: any) => this.doService.buildFileElementTree(item)),
    //);

    // Optional: Log each item if needed
    fileElements.forEach((item: any) => console.log(item));

    return fileElements;
  }
  @Get('/dobuckets')
  listBuckets() {
    this.doService.listBuckets();
  }
}
