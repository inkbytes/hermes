import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from '@controllers/app.controller';
import { PgService } from '@services/postgres.service';
import { EntitiesModule } from '@entities/entities.module';
import { LogMessage } from '@entities/messor';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import * as config from '@environments/dev/config.json';
import { AppService } from '@services/app.service'; // Adjust the path as necessary
import { DigitalOceanService } from './digitalocean/digitalocean.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      // Load and parse the JSON config file
      load: [() => config],
    }),
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        exchanges: [
          {
            name: 'hermes',
            type: 'fanout',
          },
        ],
        uri: configService.get<string>('rabbitmq.uri'),
        connectionInitOptions: { wait: false },
      }),
      inject: [ConfigService],
    }),
    ClientsModule.registerAsync([
      {
        name: 'RABBITMQ_SERVICE',
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [configService.get<string>('rabbitmq.uri')],
            queue: configService.get<string>('rabbitmq.queues.logging'),
            queueOptions: {
              durable: true,
              noAck: false,
            },
          },
        }),
        inject: [ConfigService],
      },
    ]),
    EntitiesModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>('database.host'),
        port: configService.get<number>('database.port'),
        username: configService.get<string>('database.username'),
        password: configService.get<string>('database.password'),
        database: configService.get<string>('database.database'),
        entities: [LogMessage],
        synchronize: true, // For development only
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([LogMessage]),
    EntitiesModule,
  ],
  controllers: [AppController],
  providers: [
    PgService,
    AppService,
    LogMessage,
    ConfigService,
    DigitalOceanService,
  ],
})
export class AppModule {}
