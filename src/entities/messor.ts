// your.entity.ts
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Messor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dataField1: string;

  @Column()
  dataField2: string;

  // Add more fields as necessary
}

@Entity('log_messages')
export class LogMessage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp without time zone', nullable: true })
  timestamp: Date;

  @Column({ type: 'varchar', length: 255, nullable: true })
  app_name: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  level: string;

  @Column({ type: 'text', nullable: true })
  message: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  func_name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  module: string;

  @Column({ type: 'text', nullable: true })
  args: string;
  // Static method to create an instance from a JSON object
  static createFromJSON(json: Partial<LogMessage>): LogMessage {
    const entity = new LogMessage();
    Object.assign(entity, json);
    return entity;
  }
}
