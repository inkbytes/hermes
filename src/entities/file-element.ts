import { S3ObjectEntity } from './s3entity';

export class FileElement {
  id?: string; // Unique identifier for each element
  name: string; // Name of the file or folder
  type: 'folder' | 'file'; // Type of the element
  parent?: FileElement; // Reference to the parent element, if any
  children?: FileElement[]; // Only for folders, to hold child elements
  expanded?: boolean | undefined;
  s3object?: S3ObjectEntity;

  constructor(
    name: string,
    type: 'folder' | 'file',
    parent?: FileElement,
    children?: FileElement[],
    id?: string,
    expanded?: boolean,
    s3object?: any | S3ObjectEntity, // Add this line
  ) {
    this.name = name;
    this.type = type;
    this.parent = parent;
    this.children = children;
    this.id = id;
    this.expanded = expanded;
    this.s3object = s3object;
  }
}
