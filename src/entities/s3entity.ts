// Assuming you have an Owner entity defined

import { FileElement } from './file-element';

export class S3ObjectEntity {
  Key: string;
  LastModified: Date;
  ETag: string;
  ChecksumAlgorithm: string[];
  Size: number;
  StorageClass: string;
  Owner: OwnerEntity; // You might need to define a type or class for the Owner object if it has a specific structure

  constructor(partial: Partial<S3ObjectEntity>) {
    Object.assign(this, partial);
  }
  // Enhanced method to convert to FileElement
  toFileElement(): FileElement {
    // Split the key to analyze its structure for name and potential hierarchy
    const keyParts = this.Key.split('/');
    const name = keyParts.pop() || keyParts.pop() || ''; // Handle trailing slash for folders
    const type = this.Key.endsWith('/') ? 'folder' : 'file';
    const id = this.ETag; // Using ETag as a unique identifier
    const s3object = Object(this) as S3ObjectEntity;
    // This method does not handle setting the parent or children as it lacks the full context
    // Necessary logic to establish parent-child relationships would need a broader context
    return new FileElement(name, type, undefined, [], id, false, s3object);
  }
}
export class OwnerEntity {
  ID: string;
  DisplayName: string;

  constructor(id: string, displayName: string) {
    this.ID = id;
    this.DisplayName = displayName;
  }
}
