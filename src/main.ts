import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
//import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  /*app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: ['amqp://guest:guest@kloudsix.io:5672'],
      queue: 'hermes',
      queueOptions: {
        durable: true,
      },
    },
  });*/
  app.enableCors();
  await app.listen(3000);
}
bootstrap().then(() => {});
