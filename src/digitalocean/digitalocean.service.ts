import { Injectable } from '@nestjs/common';
import AWS from 'aws-sdk';
import { S3ObjectEntity } from '../entities/s3entity';
import { FileElement } from '../entities/file-element';

@Injectable()
export class DigitalOceanService {
  private s3: AWS.S3;

  constructor() {
    this.s3 = new AWS.S3({
      endpoint: 'https://nyc3.digitaloceanspaces.com',
      accessKeyId: 'DO00WFG32VVT6Y69ENT7',
      secretAccessKey: 'wmll7MNZVLgY61WTvtJIC93J5tOEZ1UU9yBFivRLhuM',
      region: 'nyc3',
    });
  }
  async buildFileElementTree(
    data: any,
    parent?: FileElement,
  ): Promise<FileElement> {
    const fileElement = new FileElement(
      data.name,
      data.type,
      parent,
      [],
      data.id,
      data.expanded,
      data.s3object,
    );

    if (data.children && data.type === 'folder') {
      fileElement.children = data.children.map((child: any) =>
        this.buildFileElementTree(child, fileElement),
      );
    }
    return fileElement;
  }
  async buildHierarchyFromPaths(fileData: any[]): Promise<FileElement[]> {
    const root = new FileElement('root', 'folder'); // Assuming 'root' is the top-level directory

    fileData.forEach((file) => {
      let currentFolder = root;
      const parts = file.name.split('/');

      parts.forEach((part, index) => {
        if (index < parts.length - 1) {
          // It's a folder
          let nextFolder = currentFolder.children?.find((f) => f.name === part);
          if (!nextFolder) {
            nextFolder = new FileElement(part, 'folder', currentFolder);
            currentFolder.children = [
              ...(currentFolder.children || []),
              nextFolder,
            ];
          }
          currentFolder = nextFolder;
        } else {
          // It's a file
          const newFile = new FileElement(
            part,
            'file',
            currentFolder,
            [],
            file.id,
            file.expanded,
            file.s3object,
          );
          currentFolder.children = [...(currentFolder.children || []), newFile];
        }
      });
    });

    return root.children || [];
  }

  async listBuckets() {
    this.s3.listBuckets((err, data) => {
      if (err)
        console.log(err, err.stack); // an error occurred
      else {
        for (const _bucket of data.Buckets) {
          this.s3.listDirectoryBuckets(
            {
              // ListDirectoryBucketsRequest
              ContinuationToken: 'STRING_VALUE',
              MaxDirectoryBuckets: Number('int'),
            },
            (err, data) => {
              if (err) console.log(err, err.stack);
              else console.log(data); // successful response
            },
          );
        }
      } // successful response
    });
  }
  async listFiles(): Promise<FileElement[]> {
    const bucketParams = {
      Bucket: 'inkbytes',
    };

    try {
      const data = await this.s3.listObjectsV2(bucketParams).promise();
      const files: FileElement[] = data.Contents.map((obj) => {
        return new S3ObjectEntity(
          obj as Partial<S3ObjectEntity>,
        ).toFileElement();
      });
      //const _files = await this.buildHierarchyFromPaths(files);
      console.log(files);
      return files;
    } catch (err) {
      console.error('Error', err);
      throw err; // Rethrow the error to be handled by the caller
    }
  }

  async uploadFile(buffer: Buffer, filename: string): Promise<string> {
    const params = {
      Bucket: 'your-bucket-name',
      Key: filename,
      Body: buffer,
    };
    const { Location } = await this.s3.upload(params).promise();
    return Location;
  }
}
