import { Module } from '@nestjs/common';
import { DigitalOceanService } from './digitalocean.service';

@Module({
  imports: [],
  controllers: [], // Add your controllers here
  providers: [DigitalOceanService], // Register your service here
})
export class DigitaloceanModule {}
