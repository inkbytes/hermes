import { FileElement } from '@entities/file-element';
import { S3ObjectEntity } from '@entities/s3entity';
import { Injectable } from '@nestjs/common';
import AWS from 'aws-sdk';

@Injectable()
export class DigitalOceanService {
  private s3: AWS.S3;

  constructor() {
    this.s3 = new AWS.S3({
      endpoint: 'https://nyc3.digitaloceanspaces.com',
      accessKeyId: 'DO00WFG32VVT6Y69ENT7',
      secretAccessKey: 'wmll7MNZVLgY61WTvtJIC93J5tOEZ1UU9yBFivRLhuM',
      region: 'nyc3',
    });
  }
  async listFiles(): Promise<FileElement[]> {
    const bucketParams = {
      Bucket: 'inkbytes',
    };

    try {
      const data = await this.s3.listObjects(bucketParams).promise();
      const files: FileElement[] = data.Contents.map((obj) => {
        return new S3ObjectEntity(
          obj as Partial<S3ObjectEntity>,
        ).toFileElement();
      });
      console.log(files);
      return files;
    } catch (err) {
      console.error('Error', err);
      throw err; // Rethrow the error to be handled by the caller
    }
  }
  async buildFileElementTree(
    data: any,
    parent?: FileElement,
  ): Promise<FileElement> {
    const fileElement = new FileElement(
      data.name,
      data.type,
      parent,
      [],
      data.id,
      data.expanded,
      data.s3object,
    );

    if (data.children && data.type === 'folder') {
      fileElement.children = data.children.map((child: any) =>
        this.buildFileElementTree(child, fileElement),
      );
    }
    return fileElement;
  }

  async inferAndBuildFolderStructure(): Promise<FileElement[]> {
    const files = await this.listFiles();
    const rootFolders: { [key: string]: FileElement } = {};

    files.forEach((file) => {
      const pathParts = file.name.split('/');
      let currentPath = '';
      let parentFolder: FileElement = null;

      // Infer folders from path
      pathParts.forEach((part, index) => {
        if (index < pathParts.length - 1) {
          // Is a folder
          currentPath += `${part}/`;
          if (!rootFolders[currentPath]) {
            const newFolder = new FileElement(
              part,
              'folder',
              parentFolder,
              [],
              undefined,
              false,
            );
            rootFolders[currentPath] = newFolder;
            if (parentFolder) {
              parentFolder.children = [
                ...(parentFolder.children || []),
                newFolder,
              ];
            }
          }
          parentFolder = rootFolders[currentPath];
        } else {
          // Is a file
          const fileElement = new FileElement(
            part,
            'file',
            parentFolder,
            [],
            file.id,
            false,
            file.s3object,
          );
          if (parentFolder) {
            parentFolder.children = [
              ...(parentFolder.children || []),
              fileElement,
            ];
          }
        }
      });
    });

    // Return only root level folders and files
    return Object.values(rootFolders).filter((folder) => !folder.parent);
  }
}
