package entities

import (
	"gorm.io/gorm"
)

type LogMessage struct {
	gorm.Model
	ID      uint `json:"id" gorm:"<-:create;primaryKey;type:INTEGER;autoIncrement;"`
	Pattern string
	Data    LogMessageDetails `gorm:"embedded:data"`
}

type LogMessageDetails struct {
	Timestamp string `json:"timestamp"` // Adjusted to time.Time
	AppName   string `json:"app_name"`
	Level     string `json:"level"`
	Message   string `json:"message"`
	FuncName  string `json:"funcName"`
	Module    string `json:"module"`
	Args      string `json:"args"`
}
