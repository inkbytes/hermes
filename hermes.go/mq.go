package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"hermes/communications"
	"hermes/utils"
	"hermes/utils/loggers"
	"html/template"
	"io"
	"log"
	"os"

	"github.com/streadway/amqp"
)

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	// Load configuration
	config, err := utils.LoadConfig("./config.json")
	if err != nil {
		// Handle error
		panic(err) // Example error handling
	}
	// Initialize custom logger
	customLogger := &loggers.CustomLogger{}

	// Add file log handler
	file, err := os.OpenFile("./log.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("failed to create log file: %v", err)
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			// Handle error if necessary
		}
	}(file)
	//customLogger.AddHandler(&loggers.FileLogHandler{File: file})

	// Add screen log handler
	//customLogger.AddHandler(&loggers.ScreenLogHandler{})

	// Add stream log handler (stdout for demonstration)
	//customLogger.AddHandler(&loggers.StreamLogHandler{Writer: os.Stdout})

	// Initialize RabbitMQ client
	client := communications.NewRabbitMQClient(config.RabbitMQ.URI)

	// Ensure connection and channel are closed when the function exits
	defer func(Conn *amqp.Connection) {
		err := Conn.Close()
		if err != nil {

		}
	}(client.Conn) // Assumes Close() is a method that closes both the connection and channel
	// Declare a queue
	queue := client.DeclareQueue("hermes")
	client.SetQoS()
	//server := communications.WebServer()
	//server.GET("/ws", hello)
	//server.Static("/", "./public")
	//server.GET("/pages/examples", Hello)
	//server.Renderer = t
	//server.Logger.Fatal(server.Start(":3000"))

	dsnString := fmt.Sprintf("user=%s password=%s host=%s port=%d dbname=%s sslmode=disable",
		config.Database.Username, config.Database.Password, config.Database.Host, config.Database.Port,
		config.Database.Database)
	db, err := gorm.Open(postgres.Open(dsnString), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	} else {

	}
	//err = db.AutoMigrate(&entities.LogMessage{})
	if err != nil {
		return
	}

	f, err := os.OpenFile("notes.txt", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal(err)
	}
	customLogger.AddHandler(&loggers.FileLogHandler{File: f})
	customLogger.AddHandler(&loggers.DatabaseLogHandler{Db: db})
	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

	decoratedFunc := loggers.LogFuncActivity(client.StartConsuming).(func(string, gorm.DB))
	decoratedFunc(queue.Name, *db)
	// Wait indefinitely (or until an interrupt signal is received)
	stopChan := make(chan bool)
	<-stopChan
}

var (
	upgrader = websocket.Upgrader{}
)

func hello(c echo.Context) error {
	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}
	defer ws.Close()

	for {
		// Write
		err := ws.WriteMessage(websocket.TextMessage, []byte("Hello, Client!"))
		if err != nil {
			c.Logger().Error(err)
		}

		// Read
		_, msg, err := ws.ReadMessage()
		if err != nil {
			c.Logger().Error(err)
		}
		fmt.Printf("%s\n", msg)
	}
}
