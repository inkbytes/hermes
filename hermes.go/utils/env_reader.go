package utils

import (
	"encoding/json"
	"os"
)

// Define a generic type for the Queue, although not strictly necessary here, for demonstration.
type QueueItem[T any] struct {
	Key   string
	Value T
}

// RabbitMQ configuration section
type RabbitMQConfig struct {
	URI    string              `json:"uri"`
	Queues []map[string]string `json:"queues"` // Using a map to represent variable key-value pairs
}

// Database configuration section
type DatabaseConfig struct {
	Type     string `json:"type"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Username string `json:"username"`
	Password string `json:"password"`
	Database string `json:"database"`
}

// Overall configuration structure
type Config struct {
	RabbitMQ RabbitMQConfig `json:"rabbitmq"`
	Database DatabaseConfig `json:"database"`
}

func LoadConfig(configFilePath string) (*Config, error) {
	var config Config
	file, err := os.Open(configFilePath)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {

		}
	}(file)

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
