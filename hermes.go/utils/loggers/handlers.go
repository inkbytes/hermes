package loggers

import (
	"encoding/json"
	"fmt"
	"gorm.io/gorm"
	"hermes/entities"
	"io"
	"log"
	"os"
)

// LogHandler defines the interface for log handlers
type LogHandler interface {
	Log(message string)
}

// FileLogHandler writes logs to a file
type FileLogHandler struct {
	File *os.File
}

// StreamLogHandler writes logs to any io.Writer stream
// This can be adapted for websockets by passing a websocket.Conn as the writer
type StreamLogHandler struct {
	Writer io.Writer // Now it can write to any io.Writer, not just files
}
type ScreenLogHandler struct{}
type DatabaseLogHandler struct {
	Db *gorm.DB
}

func (h *FileLogHandler) Log(message string) {
	_, err := h.File.WriteString(message + "\n")
	// h.Log(message) // This line should be removed
	if err != nil {
		log.Printf("error writing to file: %v", err)
	}
}
func (h *ScreenLogHandler) Log(message string) {
	log.Println(message)

}
func (h *DatabaseLogHandler) Log(message string) {
	var logMsg entities.LogMessage

	if err := json.Unmarshal([]byte(message), &logMsg); err != nil {
		log.Printf("Error unmarshalling JSON: %v", err)
		return // Exit the function without terminating the application
	}

	if result := h.Db.Create(&logMsg); result.Error != nil {
		log.Printf("Error inserting log message into database: %v", result.Error)
	} else {
		fmt.Println(message)
		fmt.Println("Done")
		fmt.Println(result)
	}

}
func (h *StreamLogHandler) Log(message string) {
	_, err := h.Writer.Write([]byte(message))
	if err != nil {
		log.Printf("error writing to stream: %v", err)
	}
}
