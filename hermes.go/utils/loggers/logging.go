package loggers

import (
	"fmt"
	"log"
	"reflect"
	"sync"
	"time"
)

// LogRecord represents a log message with contextual information
type LogRecord struct {
	Message   string
	Timestamp time.Time
	Level     string
	// Add other fields as needed
}

// SyslogFormatter formats logs in Syslog-compatible format
type SyslogFormatter struct {
	AppName string
}

// Format a log record into a Syslog-compatible string
func (f SyslogFormatter) Format(record LogRecord) string {
	return fmt.Sprintf("%s %s: %s", record.Timestamp.Format(time.RFC3339), f.AppName, record.Message)
}

type Logger interface {
	Log(message string)
}
type BasicLogger struct{}

func (l *BasicLogger) Log(message string) {
	// Core loggers logic, e.g., to stdout
	fmt.Println(message)
}

type CustomLogger struct {
	Handlers []LogHandler
	wg       sync.WaitGroup
	mu       sync.Mutex // Protects access to Handlers
}

func (logger *CustomLogger) AddHandler(handler LogHandler) {
	logger.mu.Lock() // Ensure exclusive access to Handlers
	logger.Handlers = append(logger.Handlers, handler)
	logger.mu.Unlock()
}

func (logger *CustomLogger) Log(message string) {
	logger.mu.Lock() // Prevent modification while iterating
	handlers := logger.Handlers
	logger.mu.Unlock()

	for _, handler := range handlers {
		logger.wg.Add(1)
		go func(h LogHandler) {
			defer logger.wg.Done()
			h.Log(message)
		}(handler)
	}

	logger.wg.Wait()
}

type TimestampLoggerDecorator struct {
	Logger Logger
}

func (d *TimestampLoggerDecorator) Log(message string) {
	decoratedMessage := fmt.Sprintf("%s: %s", time.Now().Format(time.RFC3339), message)
	d.Logger.Log(decoratedMessage)
}

type LogLevelLoggerDecorator struct {
	Logger   Logger
	LogLevel string
}

func (d *LogLevelLoggerDecorator) Log(message string) {
	decoratedMessage := fmt.Sprintf("[%s] %s", d.LogLevel, message)
	d.Logger.Log(decoratedMessage)
}

// LogFuncActivity decorates any function with loggers of its activity
func LogFuncActivity(fn interface{}) interface{} {
	fnVal := reflect.ValueOf(fn)
	if fnVal.Kind() != reflect.Func {
		log.Println("LogFuncActivity: provided argument is not a function")
		return nil
	}

	return reflect.MakeFunc(fnVal.Type(), func(args []reflect.Value) (results []reflect.Value) {
		log.Println("Starting function execution...")

		// Execute the function
		results = fnVal.Call(args)

		log.Println("Finished function execution.")
		return results
	}).Interface()
}
