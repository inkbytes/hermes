package communications

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gorm.io/gorm"
	"hermes/entities"
	"log"
)

type RabbitMQClient struct {
	Conn    *amqp.Connection
	channel *amqp.Channel
}

func NewRabbitMQClient(url string) *RabbitMQClient {
	client := &RabbitMQClient{}
	client.Connect(url)
	client.OpenChannel()
	return client
}

func (client *RabbitMQClient) Connect(url string) {
	conn, err := amqp.Dial(url)
	handleError(err, "Can't connect to AMQP")
	client.Conn = conn
}

func (client *RabbitMQClient) OpenChannel() {
	channel, err := client.Conn.Channel()
	handleError(err, "Can't create a channel")
	client.channel = channel
}

func (client *RabbitMQClient) DeclareQueue(name string) amqp.Queue {
	queue, err := client.channel.QueueDeclare(name, true, false, false, false, nil)
	handleError(err, "Could not declare queue")
	return queue
}

func (client *RabbitMQClient) SetQoS() {
	err := client.channel.Qos(1, 0, false)
	handleError(err, "Could not configure QoS")
}

func (client *RabbitMQClient) StartConsuming(queueName string, db gorm.DB) {
	autoAck, exclusive, noLocal, noWait := false, false, false, false
	messages, err := client.channel.Consume(
		queueName,
		"",
		autoAck,
		exclusive,
		noLocal,
		noWait,
		nil,
	)

	handleError(err, "Could not register consumer")

	go func() {
		for d := range messages {
			var logMsg entities.LogMessage
			var msg = string(d.Body)
			if err := json.Unmarshal([]byte(msg), &logMsg); err != nil {
				log.Fatalf("Error unmarshalling JSON: %v", err)
			}
			if err := d.Ack(false); err != nil {
				log.Printf("Error acknowledging message : %s", err)
			} else {
				log.Printf("Acknowledged message")
			}
			if result := db.Create(&logMsg); result.Error != nil {
				log.Printf("Error inserting log message into database: %v", result.Error)
			} else {

				fmt.Println("Done")
				fmt.Println(result)
			}
		}
	}()
}

func handleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
