package communications

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func WebServer() *echo.Echo {

	app := echo.New()
	app.Use(middleware.Logger())
	app.Use(middleware.Recover())

	//server.GET("/ws", hello)
	//defer app.Logger.Fatal(app.Start(":3000"))
	return app
}
